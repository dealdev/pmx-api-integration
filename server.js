// server.js
// BASE SETUP
// =============================================================================

// call the packages we need
var express = require('express');        // call express
var bodyParser = require('body-parser');
var app = express();                 // define our app using express
var pmx = require('pmx');
var url = require('url');
var https = require('https');
var fs = require('fs');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, JSESSIONID");
    next();
});

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/pmx)
router.post('/', function(req, res) {
    var urlParts = url.parse(req.url, true);
    var urlQuery = urlParts.query;

    if (urlQuery.JSESSIONID && urlQuery.activity) {
        pmx.emit(urlQuery.activity, req.body);
        res.json({message: "JSESSIONID provided", urlQuery: urlQuery, req: req.body});
        console.info("pmx event has been emited !");
        console.info("activity : " + urlQuery.activity);
        console.info("payload : " + req.body);
        console.info("stringify payload : " + JSON.stringify(req.body));
    } else if (!urlQuery.JSESSIONID && !urlQuery.activity) {
        console.error("There's no activity and JSESSIONID provided !");
        res.json({message: "There's no activity and JSESSIONID provided !"});
    } else if (!urlQuery.activity) {
        console.error("There's no activity provided !");
        res.json({message: "There's no activity provided !"});
    } else if (!urlQuery.JSESSIONID) {
        console.error("There's no JSESSIONID provided !");
        res.json({message: "There's no JSESSIONID provided !"});
    }

});

app.use(pmx.expressErrorHandler());

var privateKey = fs.readFileSync('STAR_dealservices_nl.key', 'utf-8');
var certificate = fs.readFileSync('STAR_dealservices_nl.crt', 'utf-8');
var credentials = {key: privateKey, cert: certificate};

app.use('/pmx', router);
//app.listen(port);
https.createServer(credentials, app).listen(port);
console.log('pmx api integration has start on port : ' + port);